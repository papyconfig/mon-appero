def findEulerianCircuit(nodes, start, adjList):
    """
        :param nodes: List of nodes.
        :param start: Source node.
        :param adjList: Adjacency list of each node.
        :return: Find the Eulerian circuit from a source node.
    """
    circuit = []

    if len(adjList) == 0:
        return circuit

    curr_path = [start]
    while curr_path:
        curr_v = curr_path[-1]

        if adjList[nodes.index(curr_v)]:
            next_v = adjList[nodes.index(curr_v)].pop(0)
            curr_path.append(next_v)
        else:
            circuit.append(curr_path.pop())
    circuitEdges = []
    length = len(circuit)
    for i in range(length - 2, -1, -1):
        circuitEdges.append((circuit[i + 1], circuit[i]))

    return circuitEdges