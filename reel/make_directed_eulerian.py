def matrix_graph(nodes, edges):
    mat = [([0] * len(nodes)) for _ in range(len(nodes))]
    for e in edges:
        index_src = nodes.index(e[0])
        index_dst = nodes.index(e[1])
        mat[index_src][index_dst] += 1
    return mat


def is_eulerian(mat):
    for i in range(len(mat)):
        in_degree = 0
        out_degree = 0
        for j in range(len(mat)):
            in_degree += mat[j][i]
            out_degree += mat[i][j]

        if in_degree != out_degree:
            return False
    return True


def modify_mat(mat):
    nb = len(mat[0])
    i = 0
    while i < nb:
        # Calcul des degrees d'un sommet
        in_degree = 0
        out_degree = 0
        for j in range(nb):
            in_degree += mat[j][i]
            out_degree += mat[i][j]

        is_added = False
        if in_degree < out_degree:
            x = nb - 1
            while x > i and mat[x][i] != 0:
                x -= 1

            if x > i:
                is_added = True
                mat[x][i] += (out_degree - in_degree)

            if not is_added:
                x = i - 1
                while x > -1 and mat[x][i] != 0:
                    x -= 1

                if x > -1:
                    is_added = True
                    mat[x][i] += (out_degree - in_degree)
                    i = -1

        elif in_degree > out_degree:
            x = nb - 1
            while x > i and mat[i][x] != 0:
                x -= 1

            if x > i:
                is_added = True
                mat[i][x] += (in_degree - out_degree)

            if not is_added:
                x = i - 1
                while x > -1 and mat[i][x] != 0:
                    x -= 1

                if x > -1:
                    is_added = True
                    mat[i][x] += (in_degree - out_degree)
                    i = -1

        else:
            is_added = True

        if not is_added:
            print("Une personne a jugé utile de créer une impasse à sens unique !\nTanpis pour cette route !")
            for j in range(nb):
                mat[i][j] = 0
                mat[j][i] = 0

        i += 1

    return mat


def kwan(mat, nodes):
    mat = modify_mat(mat)
    # Transforme la matrice en edges (en rajoute)
    edges = []
    nb = len(nodes)
    for i in range(nb):
        for j in range(nb):
            for _ in range(mat[i][j]):
                edges.append((nodes[i], nodes[j]))

    return edges


def make_directed_eulerian(nodes, edges):
    mat = matrix_graph(nodes, edges)
    euler = is_eulerian(mat)
    new_edges = edges[:]

    print("Le graph orienté est eulérien" if euler else "Le graph orienté n'est pas eulérien")

    if not euler:
        print("Formatage en cours...")
        print("Nombres d'edges initial :", len(edges))
        new_edges = kwan(mat, nodes)
        print("Nombres d'edges final :", len(edges))

        mat = matrix_graph(nodes, new_edges)
        euler = is_eulerian(mat)
        print("Le graph orienté est finalement eulérien" if euler else "Le graph orienté n'est toujours pas eulérien")

    return new_edges