# <center> Mon APPERO </center> #

Bienvenue dans le meilleur projet de l'année. Qui est l'optimisation des trajets des équipes de déneigement de la ville de Montréal.

Pour rappel, ce projet doit répondre aux problématiques suivantes :
* Déterminer le trajet minimal du drone lors du survol du réseau routier.
* Déterminer le trajet minimal d'un appareil de déblaiement d'une zone de la ville de Montréal.
* Proposer un modèle de coût pour les opérations de déblaiement sur l'ensemble de la ville en fonction
  du nombre de véhicules à disposition.

Vous trouverez dans ce README comment installer et exécuter le projet, mais aussi la vidéo de
présentation de notre solution.
Voici une courte présentation des fichiers et dossiers que vous trouverez dans ce projet :
- Le fichier *AUTHORS* contient la liste des auteurs de ce projet.
- Le pdf nommé *synthese.pdf* à la racine du projet est une synthèse de nos réflexions à ce problème.
- Le dossier *theoretical* contient la sous-arborescence consacrée à l’étude du cas théorique
- Le dossier *real* contient la sous-arborescence consacrée à l’étude du cas réel

Dans la partie théorique du projet, vous trouverez la partie centrale de notre projet avec les différents algorithmes
que nous avons utilisé. Nous avons créé des graphes simples et compréhensibles par l'homme afin de tester notre programme 
avant de l'appliquer sur la ville de Montréal. Ces graphes sont disponibles dans le dossier *tests*. Notre programme renvoi 
les différents itinéraires des véhicules directement dans le terminal.

Dans la partie réelle, vous trouverez l'application concrète de notre programme sur le cas de la ville de Montréal.
Celui-ci créé alors des fichiers `VehiculeX.txt` où X est le numéro du véhicule en question avec l'itinéraire de ce dernier.
Ces fichiers contiennent également des infos complémentaires tels que les points de départ et d'arrivée, le numéro du camion,
mais aussi des infos sur la distance totale et des estimations sur la consommation en essence.

## Instructions d'installation : ##

### Installation via un bash ###

* Installer Python: `$ sudo apt-get install python3.9` ou `$ sudo pacman -S python`
* Installer OSMnx: `python -m pip install OSMnx`


## Instructions d'exécution : ##

**Cas théorique :**

1. Rendez-vous dans le dossier theoretical.
1. Modifiez éventuellement les paramètres dans le fichier `theoretical.py`.
1. Exécutez le fichier `theoretical.py`, via un bash avec `python theoretical.py` soit directement dans un IDE comme idea.

**Cas réel :**

1. Rendez-vous dans le dossier reel.
1. Modifiez éventuellement les paramètres dans le fichier `real.py`.
1. Exécutez le fichier `real.py`, via un bash avec `python real.py` soit directement dans un IDE comme idea.
1. Récupérez les résultats dans les fichiers `VehiculeX.txt` où X est le numéro du camion en question.
1. Les résultats pour les trottoirs sont dans les fichiers `VehiculeWalkX.txt` où X est le numéro du camion.

**Cas de démonstration :**
1. Exécutez le fichier `main.py`, via un bash avec `python main.py <name> <state> <number> [Essence cost] [-t]`. Les arguments dans l'ordre sont:
   1. Le nom de la ville
   1. Nom du pays
   1. Nombre de véhicules
   1. Prix de l'essence
   1. Parcourir les trottoirs
1. Récupérez les résultats dans les fichiers `VehiculeX.txt` où X est le numéro du camion en question.
1. Les résultats pour les trottoirs, si l'option est activée, sont dans les fichiers `VehiculeWalkX.txt` où X est le numéro du camion.

## LINKS ##

Voici le [lien](https://www.youtube.com/watch?v=KMaewujYOrs) vers la vidéo de présentation de notre solution.
