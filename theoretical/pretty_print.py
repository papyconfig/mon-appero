def pretty_print(recap_tab, vehicles_tab, vehicleNumber):
    print('{0:^60}'.format("RECAP"))
    print("Total véhicules: " + str(vehicleNumber))
    print()

    form = "{0:^15}{1:^20}{2:^20}"

    for val in recap_tab:
        print(form.format(*val))

    print()
    print('{0:^60}'.format("DETAIL"))

    i = 1
    for val in vehicles_tab:
        print('{0:^25}'.format("Véhicule " + str(i)))

        for road in val:
            print(str(road[0]) + " -> " + str(road[1]))

        print()
        i += 1


def divide(L, n):
    R = dict()
    index = 0
    while L:
        length = len(L) // (n - index)
        t = []
        for i in range(length):
            t.append(L.pop(0))
        R[index] = t
        index += 1

    return R


def main_print(circuit:list, vehiclesNumber:int = 1, EssenceCost = 2.36):
    recap_vehicles = [["Véhicule", "Position de départ", "Position d'arrivée"]]
    vehicles_detail = []
    truckNumber = 1

    circuit_each_vehicle = divide(circuit, vehiclesNumber)

    # iterate the number of vehicle we got
    for vehicle in circuit_each_vehicle:
        # Create the recap for the current vehicle
        initialNode = circuit_each_vehicle[vehicle][0][0] # first node
        finalNode = circuit_each_vehicle[vehicle][-1][1] # change to the final node

        roadDetail = []
        for road in circuit_each_vehicle[vehicle]:
            roadDetail.append((road[0], road[1]))

        current_vehicle_recap = [truckNumber, initialNode, finalNode]
        recap_vehicles.append(current_vehicle_recap)

        vehicles_detail.append(roadDetail)

        # restore the values
        truckNumber += 1

    # call the pretty print with all values
    pretty_print(recap_vehicles, vehicles_detail, vehiclesNumber)

def drone_print(circuit:list):
    print('{0:^60}'.format("DETAIL"))

    for val in circuit:
        print(str(val[0]) + "->" + str(val[1]))

    print()