def loadGraph(filename: str):
    """
        :param filename: Name of the file that contains the saved graph.
        :return: The list of nodes and the list of edges of the saved graph.
    """
    nodes = []
    edges = []
    with open(filename, 'r') as f:
        line = f.readline().split('[')[-1].split(']')[0]
        for v in line.split(', '):
            nodes.append(int(v))

        line = f.readline().split('[')[-1].split(']')[0]
        for t in line.split(', '):
            v = []
            for e in t[1:-1].split(','):
                try:
                    v.append(int(e))
                except:
                    v.append(e)
            edges.append(tuple(v))

    return nodes, edges


def makeAdjList(nodes, edges, directed=True):
    adjList = [[] for _ in range(len(nodes))]
    for edge in edges:
        src_index = nodes.index(edge[0])
        adjList[src_index].append(edge[1])

        if not directed:
            src_index = nodes.index(edge[1])
            adjList[src_index].append(edge[0])

    return adjList


def makeAdjListFromDirectedToUndirected(nodes: list, edges: list):
    adjList = [[] for _ in range(len(nodes))]
    for edge in edges:
        src_index = nodes.index(edge[0])
        dst_index = nodes.index(edge[1])

        if nodes[dst_index] not in adjList[src_index] and nodes[src_index] not in adjList[dst_index]:
            adjList[src_index].append(nodes[dst_index])
            adjList[dst_index].append(nodes[src_index])

    return adjList


def oddNodes(nodes, adjList):
    """
    :param nodes: List of nodes.
    :param adjList: Adjacency list of each node.
    :return: A list of all odd nodes in the graph.
    """
    odd = []

    for i in range(len(adjList)):
        if len(adjList[i]) & 1:
            odd.append(nodes[i])

    return odd


def makeEdgesFromAdjList(nodes, adjList):
    edges = []

    for i in range(len(adjList)):
        src = nodes[i]
        for dst in adjList[i]:
            if (src, dst) not in edges and (dst, src) not in edges:
                edges.append((src, dst))

    return edges


def makeDot(filename: str, nodes: list, edges: list, directed: bool=True, highlight: list = [], higlight_nodes: list = []):
    dot = "digraph {\nrankdir=LR\n" if directed else "graph {\nrankdir=LR\n"
    form = "{} -> {} [{} {}];\n" if directed else "{} -- {} [{} {}];\n"
    for node in nodes:
        color = "color=\"red\"" if node in higlight_nodes else "color=\"black\""
        dot += "{} [{}]\n".format(node, color)

    for edge in edges:
        inside = False
        for h in highlight:
            if edge[0] == h[0] and edge[1] == h[1]:
                inside = True
                break
        color = 'color="red"' if inside else 'color="black"'
        try:
            label = 'label="' + str(edge[2]) + '"'
        except:
            label = 'label=""'

        dot += form.format(edge[0], edge[1], color, label)

    dot += "}\n"

    with open(filename, 'w') as f:
        f.write(dot)