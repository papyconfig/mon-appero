def findEulerianCircuit(nodes, start, adjList):
    """
        :param nodes: List of nodes.
        :param start: Source node.
        :param adjList: Adjacency list of each node.
        :return: Find the Eulerian circuit from a source node.
    """
    circuit = []

    if len(adjList) == 0:
        return circuit

    curr_path = [start]
    while curr_path:
        curr_v = curr_path[-1]

        if adjList[nodes.index(curr_v)]:
            next_v = adjList[nodes.index(curr_v)].pop(0)
            curr_path.append(next_v)
        else:
            circuit.append(curr_path.pop())
    circuitEdges = []
    length = len(circuit)
    for i in range(length - 2, -1, -1):
        circuitEdges.append((circuit[i + 1], circuit[i]))

    return circuitEdges


def countNodesDFS(nodes, adjList, start, visited):
    """
        :param nodes: List of all nodes.
        :param adjList: Adjacency list of each node.
        :param start: Start node.
        :param visited: List of visited nodes.
        :return: Count reachable nodes from start.
    """
    count = 1
    start_index = nodes.index(start)
    visited[start_index] = True
    for i in adjList[start_index]:
        if not visited[nodes.index(i)]:
            count = count + countNodesDFS(nodes, adjList, i, visited)
    return count


def isValidNextEdge(nodes, adjList, u, v):
    """
        :param nodes: List of nodes.
        :param adjList: Adjacency list of each node.
        :param u: Start node.
        :param v: End node.
        :return: Check if u -- v is the next edge in Euler Cycle
    """
    if len(adjList[nodes.index(u)]) == 1:
        return True

    u_index = nodes.index(u)
    v_index = nodes.index(v)

    visited = [False] * len(nodes)
    count1 = countNodesDFS(nodes, adjList, u, visited)
    adjList[u_index].remove(v)
    adjList[v_index].remove(u)

    visited = [False] * len(nodes)
    count2 = countNodesDFS(nodes, adjList, u, visited)
    adjList[u_index].append(v)
    adjList[v_index].append(u)

    return count2 >= count1


def printEuler(nodes, adjList, u, L):
    """
        :param nodes: List of nodes.
        :param adjList: Adjacency list for each node.
        :param u: Start node.
        :param L: List to store path.
        :return:
    """
    for v in adjList[nodes.index(u)]:
        if isValidNextEdge(nodes, adjList, u, v):
            L.append((u, v))
            adjList[nodes.index(u)].remove(v)
            adjList[nodes.index(v)].remove(u)
            printEuler(nodes, adjList, v, L)


def fleuryAlgorithm(nodes, adjList):
    """
        :param nodes: List of nodes.
        :param adjList: Adjacency list for each node.
        :return: The path from fleury Algorithm.
    """
    L = []
    printEuler(nodes, adjList, nodes[0], L)
    return L
