def BFS(nodes, adj, src, dest, v, pred, dist):
    """
        :param nodes: list of all nodes.
        :param adj: Adjacency list for each node.
        :param src: Source node.
        :param dest: Destination node.
        :param v: Number of nodes.
        :param pred: Predecessor of each node.
        :param dist: Distance from source for each node.
        :return: True if we found the dest node from src node.
    """
    queue = []
    visited = [False for i in range(v)]

    # initially all vertices are unvisited
    for i in range(v):
        dist[i] = 1000000
        pred[i] = -1

    # now source is first to be visited and distance from source to itself should be 0
    visited[nodes.index(src)] = True
    dist[nodes.index(src)] = 0
    queue.append(src)

    # standard BFS algorithm
    while len(queue) != 0:
        u = nodes.index(queue[0])
        queue.pop(0)
        for i in range(len(adj[u])):

            if not visited[nodes.index(adj[u][i])]:
                visited[nodes.index(adj[u][i])] = True
                dist[nodes.index(adj[u][i])] = dist[u] + 1
                pred[nodes.index(adj[u][i])] = nodes[u]
                queue.append(adj[u][i])

                # We stop BFS when we find destination.
                if adj[u][i] == dest:
                    return True

    return False


def findShortestPath(nodes, adj, s, dest, v):
    """
        :param nodes: List of nodes.
        :param adj: Adjacency list for each node.
        :param s: Source node.
        :param dest: Destination node.
        :param v: Number of nodes.
        :return: The shortest path between s and dest.
    """
    pred = [-1 for _ in range(v)]
    dist = [0 for _ in range(v)]

    if not BFS(nodes, adj, s, dest, v, pred, dist):
        print("Given source and destination are not connected")

    # vector path stores the shortest path
    path = []
    crawl = dest
    path.append(crawl)

    while pred[nodes.index(crawl)] != -1:
        path.append(pred[nodes.index(crawl)])
        crawl = pred[nodes.index(crawl)]

    return path