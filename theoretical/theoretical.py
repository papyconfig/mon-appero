import helper as helper

from make_undirected_eulerian import make_undirected_eulerian
from make_directed_eulerian import make_directed_eulerian
from find_eulerian_circuit import findEulerianCircuit, fleuryAlgorithm
import pretty_print

if __name__ == "__main__":
    # Vous pouvez choisir ici le graph de test à utiliser parmis ceux proposés.
    # Il suffit alors de changer le numéro du graph
    nodes, edges = helper.loadGraph('tests/Graph5')

    # =============================================== DRONE ============================================================

    # Vérifie si le graph en non orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    drone_edges = make_undirected_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, drone_edges, False)
    drone_path = fleuryAlgorithm(nodes, adjList)

    # Print itinéraire du drone
    print("\n========================== DRONE ==========================\n")
    pretty_print.drone_print(drone_path)

    # =============================================== CAMIONS ==========================================================

    print("\n========================== UN VEHICULE ==========================\n")

    # Vérifie si le graph en orienté est eulérien.
    # dans le cas contraire, rajoute des arrêtes pour le rendre eulérien
    truck_edges = make_directed_eulerian(nodes, edges)

    adjList = helper.makeAdjList(nodes, truck_edges, True)
    truck_path = findEulerianCircuit(nodes, nodes[0], adjList)

    # Pour un seul véhicule
    pretty_print.main_print(truck_path.copy(), 1, 2.36)

    # Pour x véhicules
    print("\n========================== PLUSIEURS VEHICULES ==========================\n")
    x = 4
    pretty_print.main_print(truck_path, x, 2.36)
